class Codigo {
  final int? id;
  final String code;
  final int type;

  Codigo({
    this.id,
    required this.code,
    required this.type,
  });

  Map<String, dynamic> toMap() {
    return {'id': id, 'code': code, 'type': type};
  }

  factory Codigo.fromMap(Map<String, dynamic> map) {
    return Codigo(
        id: map['id']?.toInt() ?? 0,
        code: map['code'] ?? '',
        type: map['type'] ?? 1);
  }

  @override
  String toString() => 'Codigo(id: $id, code: $code, type: $type)';
}
