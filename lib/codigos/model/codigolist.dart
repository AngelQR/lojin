import 'package:flutter/material.dart';
import 'package:lojin/home/home.dart';
import 'package:lojin/widgets/codigosbuilder.dart';
import 'package:lojin/codigos/model/codigo.dart';
import 'package:lojin/services/database_service.dart';

class CodigoPage extends StatefulWidget {
  const CodigoPage({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _CodigoPageState createState() => _CodigoPageState();
}

class _CodigoPageState extends State<CodigoPage> {
  final DatabaseService _databaseService = DatabaseService();

  Future<List<Codigo>> _getCodigos() async {
    return await _databaseService.codigos();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Lista de tickets generados'),
        centerTitle: true,
        leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () => Navigator.pushAndRemoveUntil(
                context, HomePage.route(), (route) => false)),
      ),
      body: Center(
        child: CodigoBuilder(
          future: _getCodigos(),
        ),
      ),
    );
  }
}
