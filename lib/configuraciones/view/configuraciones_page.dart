import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:lojin/constants/constants.dart';

Future<Configuraciones> fetchConfiguracion() async {
  final response = await http.get(Uri.parse('${urlServer}conf/all/$idDisp1'));

  if (response.statusCode == 200) {
    return Configuraciones.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('No se ha podido obtener la información');
  }
}

class Configuraciones {
  final String minCortesia;
  final String minPago;
  final String minPrecio;
  final String capacidad;
  final String infracPrecio;
  final int minSalida;
  final String contImp;
  final String activo;

  const Configuraciones({
    required this.minCortesia,
    required this.minPago,
    required this.minPrecio,
    required this.capacidad,
    required this.infracPrecio,
    required this.minSalida,
    required this.contImp,
    required this.activo,
  });

  factory Configuraciones.fromJson(Map<String, dynamic> json) {
    return Configuraciones(
        minCortesia: json['min_cortesia'],
        minPago: json['min_pago'],
        minPrecio: json['min_precio'],
        capacidad: json['capacidad'],
        infracPrecio: json['infrac_precio'],
        minSalida: json['min_salida'],
        contImp: json['cont_imp'],
        activo: json['activo']);
  }
}
