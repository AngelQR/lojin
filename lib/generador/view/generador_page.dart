import 'package:flutter/material.dart';
import 'package:barcode_widget/barcode_widget.dart';
import 'package:intl/intl.dart';
import 'package:lojin/codigos/model/codigo.dart';
import 'package:lojin/services/database_service.dart';
import 'package:lojin/configuraciones/configuraciones.dart';

import '../../home/home.dart';

class GeneradorPage extends StatefulWidget {
  const GeneradorPage({Key? key}) : super(key: key);
  @override
  State<GeneradorPage> createState() => _GeneradorPageState();
}

class _GeneradorPageState extends State<GeneradorPage> {
  late Future<Configuraciones> futureConfiguracion;
  late DateTime now;
  late DateFormat formatter;
  late DateFormat formatterFull;
  late String formatted;
  late String formattedFull;
  final DatabaseService _databaseService = DatabaseService();

  @override
  void initState() {
    now = DateTime.now();
    formatter = DateFormat('yyMMddHHmmss');
    formatterFull = DateFormat('y-MM-dd HH:mm');
    formatted = formatter.format(now);
    formattedFull = formatterFull.format(now);

    futureConfiguracion = fetchConfiguracion();
    saveTicket();
    super.initState();
  }

  Future<void> saveTicket() async {
    await _databaseService.insertCodigo(Codigo(code: '53$formatted', type: 1));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Generador de tickets de parqueo'),
        centerTitle: true,
        leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () => Navigator.pushAndRemoveUntil(
                context, HomePage.route(), (route) => false)),
      ),
      body: Center(
        child: FutureBuilder<Configuraciones>(
            future: futureConfiguracion,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                DateTime terminaCortesia = now.add(
                    Duration(minutes: int.parse(snapshot.data!.minCortesia)));
                return Card(
                  child: SizedBox(
                      width: 400,
                      height: 280,
                      child: Center(
                          child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            padding: const EdgeInsets.only(
                              left: 20,
                              right: 10,
                              top: 10,
                            ),
                            alignment: Alignment.centerLeft,
                            child: RichText(
                              text: TextSpan(
                                style: const TextStyle(
                                    fontSize: 14, color: Colors.black),
                                children: [
                                  const WidgetSpan(
                                      child: Icon(
                                    Icons.timer_sharp,
                                    size: 14,
                                  )),
                                  const TextSpan(
                                    text: ' Fecha y hora de entrada: \n',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  TextSpan(
                                    text: formattedFull,
                                    style: const TextStyle(
                                        fontStyle: FontStyle.italic,
                                        color: Colors.black87,
                                        fontWeight: FontWeight.normal),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.only(
                              left: 20,
                              right: 10,
                              top: 10,
                            ),
                            alignment: Alignment.centerLeft,
                            child: RichText(
                              text: TextSpan(
                                style: const TextStyle(
                                    fontSize: 14, color: Colors.black),
                                children: [
                                  const WidgetSpan(
                                      child: Icon(
                                    Icons.timer_rounded,
                                    size: 14,
                                  )),
                                  const TextSpan(
                                    text: ' Termina el tiempo de cortesía: \n',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  TextSpan(
                                    text: formatterFull.format(terminaCortesia),
                                    style: const TextStyle(
                                        fontStyle: FontStyle.italic,
                                        color: Colors.black87,
                                        fontWeight: FontWeight.normal),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.only(
                                left: 20, right: 20, top: 10, bottom: 10),
                            alignment: Alignment.center,
                            child: RichText(
                              text: TextSpan(
                                text: 'Valor por hora (por fracción): ',
                                style: const TextStyle(
                                    fontSize: 12,
                                    fontStyle: FontStyle.italic,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black87),
                                children: <TextSpan>[
                                  TextSpan(
                                    text: '\$${snapshot.data!.minPrecio}',
                                    style: const TextStyle(
                                        fontWeight: FontWeight.normal),
                                  )
                                ],
                              ),
                            ),
                          ),
                          BarcodeWidget(
                            barcode: Barcode.itf(), // Barcode type and settings
                            data: '53$formatted', // Content
                            width: 200,
                            height: 100,
                            drawText: true,
                          )
                        ],
                      ))),
                );
              } else if (snapshot.hasError) {
                return Text('${snapshot.error}');
              }
              return const CircularProgressIndicator();
            }),
      ),
    );
  }
}
