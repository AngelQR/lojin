import 'package:flutter/material.dart';
import 'package:lojin/codigos/model/codigolist.dart';
import 'package:lojin/configuraciones/configuraciones.dart';
import 'package:lojin/generador/generador.dart';
import 'package:lojin/generadorqr/generadorqr.dart';

import 'package:lojin/plazas_disponibles/plazas_disponibles.dart';
import 'package:lojin/widgets/colors.dart';
import 'package:lojin/widgets/text.dart';
import 'package:velocity_x/velocity_x.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);
  @override
  State<HomePage> createState() => _HomePageState();

  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => const HomePage());
  }
}

class _HomePageState extends State<HomePage> with RouteAware {
  late Future<PlazasDisponibles> futurePlazasDisponibles;
  late Future<Configuraciones> futureConfiguraciones;

  @override
  void initState() {
    futurePlazasDisponibles = fetchPlazasDisponibles();
    futureConfiguraciones = fetchConfiguracion();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: const Text('Menú principal'),
          centerTitle: true,
        ),
        body: CustomScrollView(
          primary: false,
          slivers: <Widget>[
            SliverSafeArea(
                top: true,
                sliver: SliverToBoxAdapter(
                    child: Padding(
                  padding: const EdgeInsets.only(
                      left: 10, right: 10, top: 20, bottom: 10),
                  child: text('Configuraciones del parqueadero',
                      fontWeight: FontWeight.bold,
                      size: 18,
                      textColor: Colors.black),
                ))),
            SliverPadding(
                padding: const EdgeInsets.all(10),
                sliver: SliverGrid.count(
                    crossAxisSpacing: 8,
                    mainAxisSpacing: 8,
                    crossAxisCount: 2,
                    childAspectRatio: (1 / .6),
                    children: _configuraciones(
                        futureConfiguraciones, futurePlazasDisponibles))),
            SliverSafeArea(
                top: true,
                sliver: SliverToBoxAdapter(
                    child: Padding(
                  padding: const EdgeInsets.only(
                      left: 10, right: 10, top: 20, bottom: 10),
                  child: text('Opciones disponibles',
                      fontWeight: FontWeight.bold,
                      size: 18,
                      textColor: Colors.black),
                ))),
            SliverPadding(
              padding: const EdgeInsets.all(10),
              sliver: SliverGrid.count(
                crossAxisSpacing: 8,
                childAspectRatio: (1 / .3),
                mainAxisSpacing: 5,
                crossAxisCount: 1,
                children: <Widget>[
                  buildMenuItem(
                      context,
                      Icons.view_week,
                      "Generar códigos de barras",
                      const Color.fromRGBO(25, 46, 128, 1),
                      whiteColor,
                      1),
                  buildMenuItem(context, Icons.qr_code_2, "Generar códigos QR",
                      const Color.fromRGBO(176, 39, 45, 1), whiteColor, 2),
                  buildMenuItem(
                      context,
                      Icons.list,
                      "Revisar tickets generados",
                      const Color.fromRGBO(234, 201, 92, 1),
                      blackColor,
                      3),
                ],
              ),
            ),
          ],
        ));
  }
}

List<Widget> _configuraciones(futureConfiguraciones, futurePlazasDisponibles) {
  return <Widget>[
    Container(
        color: const Color.fromRGBO(234, 201, 92, 1),
        child: FutureBuilder<Configuraciones>(
            future: futureConfiguraciones,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        "Minutos de cortesía:",
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        snapshot.data!.minCortesia,
                        style: const TextStyle(
                          fontSize: 12,
                        ),
                      ),
                      const Text(
                        "Precio (fracción/hora):",
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        "\$${snapshot.data!.minPrecio}",
                        style: const TextStyle(
                          fontSize: 12,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                );
              } else if (snapshot.hasError) {
                return Text('${snapshot.error}');
              }
              return const CircularProgressIndicator();
            })),
    Container(
        height: 200,
        color: const Color.fromRGBO(176, 39, 45, 1),
        child: FutureBuilder<PlazasDisponibles>(
            future: futurePlazasDisponibles,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Icon(
                        Icons.local_parking,
                        size: 24,
                        color: Colors.white,
                      ),
                      const Text(
                        "Plazas disponibles:",
                        style: TextStyle(
                            fontSize: 14,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        snapshot.data!.capacidadTotal,
                        style: const TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                );
              } else if (snapshot.hasError) {
                return Text('${snapshot.error}');
              }
              return const CircularProgressIndicator();
            }))
  ];
}

Widget buildMenuItem(BuildContext context, IconData icon, String title,
    Color? color, Color textColor, int index) {
  return InkWell(
      child: Card(
          elevation: 0,
          color: color,
          shape: const RoundedRectangleBorder(
            side: BorderSide(
              color: Color.fromARGB(255, 221, 222, 223),
            ),
            borderRadius: BorderRadius.all(Radius.circular(4)),
          ),
          margin: const EdgeInsets.only(top: 2, bottom: 2),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(icon, color: textColor, size: 30),
                text(
                  title,
                  textColor: textColor,
                  size: 16,
                  fontWeight: FontWeight.bold,
                ),
                5.heightBox,
              ],
            ),
          )),
      onTap: () {
        switch (index) {
          case 1:
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (contex) => const GeneradorPage()));
            break;
          case 2:
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (contex) => const GeneradorqrPage()));
            break;
          case 3:
            Navigator.push(context,
                MaterialPageRoute(builder: (contex) => const CodigoPage()));
            break;
        }
      });
}
