import 'package:flutter/material.dart';
import 'package:authentication_repository/authentication_repository.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:lojin/myapp.dart';
import 'package:user_repository/user_repository.dart';
//import 'package:dcdg/dcdg.dart';

void main() async {
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(
      widgetsBinding: widgetsBinding); // Llamado del splash screen de la app
  runApp(App(
      // Llamado de la función inicial principal de la app
      // Inicialización de los repositorios para validar que el usuario esté logueado
      authenticationRepository: AuthenticationRepository(),
      userRepository: UserRepository()));
}
