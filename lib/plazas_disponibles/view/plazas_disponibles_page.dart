import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:lojin/constants/constants.dart';

Future<PlazasDisponibles> fetchPlazasDisponibles() async {
  final response = await http.get(Uri.parse('${urlServer}conf/cap/$idDisp1'));

  if (response.statusCode == 200) {
    return PlazasDisponibles.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('No se ha podido obtener la información');
  }
}

class PlazasDisponibles {
  final String capacidadTotal;
  const PlazasDisponibles({required this.capacidadTotal});

  factory PlazasDisponibles.fromJson(Map<String, dynamic> json) {
    return PlazasDisponibles(
      capacidadTotal: json['capacidad_total'],
    );
  }
}
