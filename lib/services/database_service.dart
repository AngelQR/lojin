import 'package:lojin/codigos/model/codigo.dart';
// ignore: depend_on_referenced_packages
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseService {
  // Singleton pattern
  static final DatabaseService _databaseService = DatabaseService._internal();
  factory DatabaseService() => _databaseService;
  DatabaseService._internal();

  static Database? _database;
  Future<Database> get database async {
    if (_database != null) return _database!;
    // Initialize the DB first time it is accessed
    _database = await _initDatabase();
    return _database!;
  }

  Future<Database> _initDatabase() async {
    final databasePath = await getDatabasesPath();

    // Set the path to the database. Note: Using the `join` function from the
    // `path` package is best practice to ensure the path is correctly
    // constructed for each platform.
    final path = join(databasePath, 'flutter_sqflite_database.db');

    // Set the version. This executes the onCreate function and provides a
    // path to perform database upgrades and downgrades.
    return await openDatabase(
      path,
      onCreate: _onCreate,
      version: 1,
      onConfigure: (db) async => await db.execute('PRAGMA foreign_keys = ON'),
    );
  }

  // When the database is first created, create a table to store codigos
  // and a table to store dogs.
  Future<void> _onCreate(Database db, int version) async {
    // Run the CREATE {codigos} TABLE statement on the database.
    await db.execute(
      'CREATE TABLE codigos(id INTEGER PRIMARY KEY, code TEXT, type INTEGER)',
    );
  }

  // Define a function that inserts codigos into the database
  Future<void> insertCodigo(Codigo codigo) async {
    // Get a reference to the database.
    final db = await _databaseService.database;

    // Insert the Codigo into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same codigo is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      'codigos',
      codigo.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  // A method that retrieves all the codigos from the codigos table.
  Future<List<Codigo>> codigos() async {
    // Get a reference to the database.
    final db = await _databaseService.database;

    // Query the table for all the Codigos.
    final List<Map<String, dynamic>> maps = await db.query('codigos');

    // Convert the List<Map<String, dynamic> into a List<Codigo>.
    return List.generate(maps.length, (index) => Codigo.fromMap(maps[index]));
  }

  Future<Codigo> codigo(int id) async {
    final db = await _databaseService.database;
    final List<Map<String, dynamic>> maps =
        await db.query('codigos', where: 'id = ?', whereArgs: [id]);
    return Codigo.fromMap(maps[0]);
  }

  // A method that updates a codigo data from the codigos table.
  Future<void> updateCodigo(Codigo codigo) async {
    // Get a reference to the database.
    final db = await _databaseService.database;

    // Update the given codigo
    await db.update(
      'codigos',
      codigo.toMap(),
      // Ensure that the Codigo has a matching id.
      where: 'id = ?',
      // Pass the Codigo's id as a whereArg to prevent SQL injection.
      whereArgs: [codigo.id],
    );
  }

  // A method that deletes a codigo data from the codigos table.
  Future<void> deleteCodigo(int id) async {
    // Get a reference to the database.
    final db = await _databaseService.database;

    // Remove the Codigo from the database.
    await db.delete(
      'codigos',
      // Use a `where` clause to delete a specific codigo.
      where: 'id = ?',
      // Pass the Codigo's id as a whereArg to prevent SQL injection.
      whereArgs: [id],
    );
  }
}
