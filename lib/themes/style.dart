import 'package:lojin/constants/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

ThemeData appTheme() {
  return ThemeData(
      textTheme: const TextTheme(
          bodyText1: TextStyle(
        fontFamily: 'Heebo',
      )),
      fontFamily: 'Heebo',
      cardTheme: CardTheme(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
      cupertinoOverrideTheme: const CupertinoThemeData(
          textTheme: CupertinoTextThemeData(
              actionTextStyle: TextStyle(fontFamily: 'Heebo'),
              textStyle: TextStyle(fontFamily: 'Heebo'))),
      primarySwatch: ctlPrimaryColor,
      scaffoldBackgroundColor: Colors.grey[200],
      indicatorColor: ctlPrimaryColor,
      inputDecorationTheme: InputDecorationTheme(
        labelStyle: const TextStyle(fontFamily: 'Heebo'),
        hintStyle: const TextStyle(fontFamily: 'Heebo'),
        filled: true,
        fillColor: Colors.white,
        contentPadding: const EdgeInsets.all(0),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
          borderSide: const BorderSide(width: 1, color: Colors.grey),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
          borderSide: const BorderSide(width: 1.2, color: Colors.red),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
          borderSide: const BorderSide(width: 1, color: Colors.grey),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
      ));
}
