import 'dart:convert';

import 'package:barcode_widget/barcode_widget.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:lojin/constants/constants.dart';
import 'package:velocity_x/velocity_x.dart';

class ValorPagar {
  final String dispCod;
  final String codigo;
  final String fechaIni;
  final String minutos;
  final String estado;
  final String fechaFact;
  final String tipo;
  final String estadoVehiculo;

  const ValorPagar({
    required this.dispCod,
    required this.codigo,
    required this.fechaIni,
    required this.minutos,
    required this.estado,
    required this.fechaFact,
    required this.tipo,
    required this.estadoVehiculo,
  });

  factory ValorPagar.fromJson(Map<String, dynamic> json) {
    return ValorPagar(
        dispCod: json['disp_cod'],
        codigo: json['codigo'],
        fechaIni: json['fecha_ini'],
        minutos: json['minutos'],
        estado: json['estado'],
        fechaFact: json['fecha_fact'] ?? "",
        tipo: json['tipo'] ?? "",
        estadoVehiculo: json['estado_vehiculo'] ?? "");
  }
}

class ValorPagarPage extends StatefulWidget {
  final String codigo;
  final int tipo;
  const ValorPagarPage({Key? key, required this.codigo, required this.tipo})
      : super(key: key);

  @override
  State<ValorPagarPage> createState() => _ValorPagarPageState();
}

class _ValorPagarPageState extends State<ValorPagarPage> {
  late Future<ValorPagar> futureConfiguracion;

  @override
  void initState() {
    super.initState();
    futureConfiguracion = fetchConfiguracion();
  }

  Future<ValorPagar> fetchConfiguracion() async {
    final response = await http.post(
      Uri.parse('${urlServer}control/salida/$idDisp2'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'codigo': widget.codigo,
      }),
    );

    if (response.statusCode == 200) {
      return ValorPagar.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('No se ha podido obtener la información');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Detalle de ticket generado'),
        centerTitle: true,
      ),
      body: Center(
        child: FutureBuilder<ValorPagar>(
          future: futureConfiguracion,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Card(
                  child: SizedBox(
                      width: 400,
                      height: 250,
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              padding: const EdgeInsets.only(
                                left: 20,
                                right: 10,
                                top: 10,
                              ),
                              alignment: Alignment.centerLeft,
                              child: RichText(
                                text: TextSpan(
                                  style: const TextStyle(
                                      fontSize: 14, color: Colors.black),
                                  children: [
                                    const WidgetSpan(
                                        child: Icon(
                                      Icons.timer_rounded,
                                      size: 14,
                                    )),
                                    const TextSpan(
                                      text: ' Minutos en el parqueadero: ',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    TextSpan(
                                      text: snapshot.data!.minutos,
                                      style: const TextStyle(
                                          fontStyle: FontStyle.italic,
                                          color: Colors.black87,
                                          fontWeight: FontWeight.normal),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.only(
                                left: 20,
                                right: 10,
                                top: 10,
                              ),
                              alignment: Alignment.centerLeft,
                              child: RichText(
                                text: TextSpan(
                                  style: const TextStyle(
                                      fontSize: 14, color: Colors.black),
                                  children: [
                                    const WidgetSpan(
                                        child: Icon(
                                      Icons.money,
                                      size: 14,
                                    )),
                                    const TextSpan(
                                      text: ' Estado de pago: ',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    TextSpan(
                                      // ignore: unrelated_type_equality_checks
                                      text: snapshot.data!.estado == "0"
                                          ? "sin pago.\n"
                                          : "pagado.\n",
                                      style: const TextStyle(
                                          fontStyle: FontStyle.italic,
                                          color: Colors.black87,
                                          fontWeight: FontWeight.normal),
                                    ),
                                    const WidgetSpan(
                                        child: Icon(
                                      Icons.local_parking_outlined,
                                      size: 14,
                                    )),
                                    const TextSpan(
                                      text: ' Estado del vehículo: ',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    TextSpan(
                                      // ignore: unrelated_type_equality_checks
                                      text: snapshot.data!.estadoVehiculo == "0"
                                          ? "dentro del parqueadero.\n"
                                          : "fuera del parqueadero.\n",
                                      style: const TextStyle(
                                          fontStyle: FontStyle.italic,
                                          color: Colors.black87,
                                          fontWeight: FontWeight.normal),
                                    ),
                                    const WidgetSpan(
                                        child: Icon(
                                      Icons.money,
                                      size: 14,
                                    )),
                                    const TextSpan(
                                      text: ' Valor a pagar: ',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    TextSpan(
                                      // ignore: unrelated_type_equality_checks
                                      text: _valorPagar(
                                          snapshot.data!.minutos,
                                          snapshot.data!.estado,
                                          snapshot.data!.estadoVehiculo),
                                      style: const TextStyle(
                                          fontStyle: FontStyle.italic,
                                          color: Colors.black87,
                                          fontWeight: FontWeight.normal),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            const HeightBox(20),
                            BarcodeWidget(
                              barcode: widget.tipo == 1
                                  ? Barcode.itf()
                                  : Barcode
                                      .qrCode(), // Barcode type and settings
                              data: widget.codigo, // Content
                              width: 200,
                              height: 100,
                              drawText: true,
                            ),
                          ])));
            } else if (snapshot.hasError) {
              return const Card(
                margin: EdgeInsets.all(10),
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: Text(
                    "No se ha podido obtener la información. \nAsegúrate de que el código seleccionado ha sido utilizado correctamente para el ingreso al parqueadero.",
                    textAlign: TextAlign.center,
                  ),
                ),
              );
            }

            // By default, show a loading spinner.
            return const CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}

String _valorPagar(minutos, estadoPago, estadoVehiculo) {
  double valorFinal = 0;
  double min = double.parse(minutos);
  if (estadoVehiculo == "0") {
    if (estadoPago == "0") {
      if (min > 30) {
        valorFinal = (min - 30) * 0.5;
      }
    }
  }

  return valorFinal.toString();
}
