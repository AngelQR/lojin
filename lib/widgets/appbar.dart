import 'package:flutter/material.dart';

PreferredSizeWidget appBar(
    {required double elevation,
    required Widget title,
    required Widget leading,
    required List<Widget> actions,
    required Color backgroundColor}) {
  return AppBar(
    title: title,
    leading: leading,
    actions: actions,
    elevation: elevation,
    backgroundColor: backgroundColor,
  );
}
