import 'package:flutter/material.dart';
import 'package:lojin/codigos/model/codigo.dart';
import 'package:lojin/valorpagar/valorpagar.dart';

class CodigoBuilder extends StatelessWidget {
  const CodigoBuilder({
    Key? key,
    required this.future,
  }) : super(key: key);
  final Future<List<Codigo>> future;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Codigo>>(
      future: future,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: ListView.builder(
            itemCount: snapshot.data!.length,
            itemBuilder: (context, index) {
              final codigo = snapshot.data![index];
              return _buildCodigoCard(codigo, context);
            },
          ),
        );
      },
    );
  }

  Widget _buildCodigoCard(Codigo codigo, BuildContext context) {
    return InkWell(
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Row(
              children: [
                Container(
                    height: 40.0,
                    width: 40.0,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color.fromARGB(255, 244, 244, 244),
                    ),
                    alignment: Alignment.center,
                    child: codigo.type == 1
                        ? const Icon(Icons.view_week)
                        : const Icon(Icons.qr_code_2)),
                const SizedBox(width: 20.0),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        codigo.code,
                        style: const TextStyle(
                          fontSize: 18.0,
                          color: Color.fromARGB(255, 68, 67, 67),
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      const SizedBox(height: 4.0),
                    ],
                  ),
                ),
                const SizedBox(width: 20.0),
                Container(
                    height: 40.0,
                    width: 40.0,
                    alignment: Alignment.center,
                    child: const Icon(Icons.remove_red_eye_outlined)),
              ],
            ),
          ),
        ),
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (contex) =>
                      ValorPagarPage(codigo: codigo.code, tipo: codigo.type)));
        });
  }
}
