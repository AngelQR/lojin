import 'package:flutter/material.dart';

Widget text(String text,
    {required Color textColor,
    required double size,
    FontWeight? fontWeight,
    FontStyle? style}) {
  return Text(
    text,
    textAlign: TextAlign.left,
    style: TextStyle(
        color: textColor,
        fontSize: size,
        fontWeight: fontWeight,
        fontStyle: style),
  );
}
